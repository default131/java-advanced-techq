# Java - advanced techniques course

Hacky weekly assignments

## Lab01
File-diff directory explorer; check MD5; use java9 modules; utilise **jlink**; java.nio(partially)

## Lab02
"Employee" driectory explorer; view employee records and photos (text and img files) in their directories; **weak references** (records/photos displayed in gui are free to be garbage-collected); indicate whether loaded from memory or disk

## Lab03
Quiz using a free REST API; Spring; internationalization; save language choice in package-bound **Preferences**; **language resource bundles**, implemented as both classes and files

## Lab04
Execute tasks by using dynamically loaded classes, extending a given Interface; using a **custom class loader**; access needed functionality by **reflection**

## Lab05
Process imported table data using obtained services, implementing given Interfaces; **Service Provider Interface**; export jarred service classes in a modular fashion, as well as utilising .jar META-INF settings

## Lab06
Billboards operated by a Manager, with ads supplied by Clients; entity Interfaces provided; **RMI**; exporting stubs/communication between instances using **SSL** sockets, with self-signed certs; **keystore/truststore**; custom **SslRMIClientSocketFactory**; security manager with custom **Policy** class

## Lab07
School comitee payment tracker; sqlite; **ORM** using **Hibernate**

## Lab08

## Lab09
File encryption/decryption app; RSA, AES; **signed module jar**

## Lab10
**Multi-release jar**; **native app installer**

## Lab11
**JNI**; sorting Double arrays in C++ by accessing Java object fields

## Lab12
Cellular automata Javascript implementations, dynamically loaded through the **Nashorn** engine

## Lab13
Simple invitation card generation by string replacement in supplied template files; **JavaFX** gui defined and loaded from an fxml file; app logic implemented in Javascript, solely in the fxml layout file

## Lab14
**Java Management Extension**; **MXBEAN**; controlling dummy decoration lights (gui elements) through JMX, in jconsole

## Lab15
Last short assignment; comparison of Simpson's rule implementations' accuracy , double vs **BigDecimal**
