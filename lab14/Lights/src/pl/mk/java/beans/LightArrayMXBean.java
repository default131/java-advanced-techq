package pl.mk.java.beans;

public interface LightArrayMXBean {
	public void setSeries(String series);
	public String getSeries();
	public void exclude(int index);
	public void include(int index);
	public void startStop();
}
