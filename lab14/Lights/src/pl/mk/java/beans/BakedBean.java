package pl.mk.java.beans;

import java.beans.ConstructorProperties;

import pl.mk.java.gui.Window;

public class BakedBean implements LightArrayMXBean{
	
	private String series;
	private Window w;
	
	@Override
	public void setSeries(String series) {
		this.series = series;
		w.setSeq(series);
	}

	@Override
	public String getSeries() {
		return this.series;
	}
	
	@ConstructorProperties({"series"})
	public BakedBean(Window w, String series) {
		this.w = w;
		this.series = series;
	}

	@Override
	public void exclude(int index) {
		w.exclude(index);
	}

	@Override
	public void include(int index) {
		w.include(index);
	}

	@Override
	public void startStop() {
		w.startStop();
	}
}
