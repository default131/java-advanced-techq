package pl.mk.java.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

public class Window {

	private JFrame frame;
	private pl.mk.java.gui.Window.DrawPanel drawPanel;
	
	private class DrawPanel extends JPanel {
		private static final long serialVersionUID = 1L;
		private static final int LIGHT_N = 5;
		private static final int SLEEP_MS = 2000;
		private Thread drawingThread;
		private boolean[] onSeq;// %5
		
		private boolean[] currSeq;
		private int i = 0;
		
		private boolean[] excluded = new boolean[LIGHT_N];
		private volatile Boolean stopped = false;
		
		DrawPanel() {
			setSeq("1100010000010001000001000");//TODO
			synchronized(this) {
				currSeq = getNextSubSeq();
			}
			drawingThread = new Thread(this::run);
			drawingThread.start();
		}
		
		private synchronized boolean[] getNextSubSeq() {
			boolean[] retVal = new boolean[LIGHT_N];
			if(onSeq == null) {
				Arrays.fill(retVal, false);
				return retVal;
			}
			retVal = Arrays.copyOfRange(onSeq, i, i + LIGHT_N);
			i += LIGHT_N;
			if (i + LIGHT_N > onSeq.length) {
				i = 0;
			}
			return retVal;
		}
		
		private void run() {
			try {
				while(true) {//TODO
					synchronized(this) {
						if(stopped) {
							this.wait();
						}
						currSeq = getNextSubSeq();
					}
					this.repaint();
					Thread.sleep(SLEEP_MS);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public synchronized void setSeq(String seq) {
			if (seq.length() % 5 != 0) {
				onSeq = null;
				return;
			}
			onSeq = new boolean[seq.length()];
			for(int it = 0; it < seq.length(); ++it) {
				onSeq[it] = seq.charAt(it) == '1';
			}
		}
		
		public void exclude(int index) {
			excluded[index] = true;
		}
		
		public void include(int index) {
			excluded[index] = false;
		}
		
		public void startStop() {
			synchronized (this) {
				stopped = !stopped;
				this.notify();
			}
		}
		
		@Override
		protected void paintComponent(Graphics g) {
			var dimensions = this.getSize();
			var width = dimensions.height/8;
			var padding = (dimensions.width - width * LIGHT_N) / (LIGHT_N + 1);
			
			synchronized(this) {
				for(int it = 0; it < LIGHT_N; ++it) {
					g.setColor(currSeq[it] && !excluded[it]? Color.GREEN : Color.GRAY);
					g.fillOval(padding * (it+1) + width * it,
							dimensions.height/2 - width/2,
							width, width);
				}
			}
		}
		
	}
	
	public void setSeq(String seq) {
		drawPanel.setSeq(seq);
	}
	
	public void exclude(int index) {
		drawPanel.exclude(index);
	}
	
	public void include(int index) {
		drawPanel.include(index);
	}
	
	public void startStop() {
		drawPanel.startStop();
	}
	
	/**
	 * Create the application.
	 */
	public Window() {
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Windows".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception e) {
		   
		}
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 786, 463);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(1, 0, 0, 0));
		drawPanel = new DrawPanel();
		frame.getContentPane().add(drawPanel);
	}

}
