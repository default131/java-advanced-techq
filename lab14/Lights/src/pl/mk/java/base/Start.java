package pl.mk.java.base;

import java.lang.management.ManagementFactory;

import javax.management.ObjectName;

import pl.mk.java.beans.BakedBean;
import pl.mk.java.gui.Window;

public class Start {
	public static void main(String[] args) {
		var window = new Window();
		var bean = new BakedBean(window, "1100010000010001000001000");
		try {
			ManagementFactory.getPlatformMBeanServer().registerMBean(bean,
					new ObjectName("pl.mk.java.beans:name=" + "LightArray"));
			Thread.currentThread().join();
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
		}
	}
}
