package pl.mk.java.base;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.function.Function;

public class Start {
	public static void main(String[] args) {
		var val = integrateD(Start::f);
		System.out.println(val);
		var valB = integrateB(Start::fB);
		System.out.println(valB);
	}

	private static final int PREC = MathContext.DECIMAL128.getPrecision();
	private static final MathContext CONTEXT = 
			new MathContext(PREC, RoundingMode.HALF_DOWN);
	public static double f(double x) {
		return 0.015 * (Math.pow(x, 2) - Math.pow(x, 1));
	}
	
	public static BigDecimal fB(BigDecimal x) {
		return (x.pow(2).subtract(x.pow(1))).multiply(
				new BigDecimal(0.015,CONTEXT));
	}
	
	public static double integrateD(Function<Double, Double> f) {
		double retVal = 0;
		final double dx = 1;
		final int N = 3/(int)dx;
		for(int i = 0; i < N; ++i) {
			retVal += (1/6.0)*dx*(f(i*dx) + 4*f(0.5*dx+i*dx) + f(dx+i*dx));
		}
		return retVal;
	}
	
	public static BigDecimal bg(int x) {
		return new BigDecimal(x, CONTEXT);
	}
	
	public static BigDecimal bg(double x) {
		return new BigDecimal(x, CONTEXT);
	}
	
	public static BigDecimal integrateB(Function<BigDecimal, BigDecimal> f) {
		BigDecimal retVal = bg(0);
		final BigDecimal dx = bg(0.1);
		final double N = bg(3).divide(dx, PREC, RoundingMode.HALF_DOWN).doubleValue();
		
		var sixthDx = bg(1).divide(bg(6), PREC, RoundingMode.HALF_DOWN).multiply(dx);
		for(int i = 0; i < N; ++i) {
			var f1 = f.apply(dx.multiply(bg(i), CONTEXT));
			var f2 = bg(4).multiply(f.apply(bg(0.5).multiply(dx).add(bg(i).multiply(dx))));
			var f3 = f.apply(dx.add(bg(i).multiply(dx)));
			retVal = retVal.add(sixthDx.multiply(f1.add(f2).add(f3)));
		}
		return retVal;
	}
}