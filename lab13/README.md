# Lab13 - Java + JavaFX (FXML) + Javascript
## Użycie aplikacji generującej zaproszenia z szablonów
Poprawne działanie aplikacji wymaga odpowiedniej struktury roboczego folderu. Powinien on zawierać folder *templates*, zawierający szablony.
![usg1](img/tree.png)

Szablon ma strukturę pliku tekstowego, listującego w kolejnych liniach podmieniane pola. Po wylistowaniu pól, należy przejść do nowej linii (jedna pusta) i podać właściwy szablon korzystający z pól, podanych w klamrach. Przykład poniżej. Brak nowej linii po szablonie.
![usgTemp](img/template.png)

Po uruchomieniu aplikacji należy wybrać jeden z szablonów obecnych w *templates* podczas uruchomienia. Po wyborze zaprezentowane zostaną oczekiwane pola oraz format ich podawania.
![usg2](img/select_template.gif)

Po wyborze należy adekwatnie wprowadzić dane i zatwierdzić. Wygenerowane zaproszenie można skopiować.
![usg3](img/generate_inv.gif)