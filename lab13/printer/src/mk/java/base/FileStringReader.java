package mk.java.base;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileStringReader {
	public static String readFile(String filePath) {
		try {
			return Files.readString(Paths.get(filePath));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
