package mk.java.base;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Start extends Application {
	public static void launch(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		var stream = getClass().getResourceAsStream("FX.fxml");
		VBox root = (VBox) loader.load(stream);

		Scene scene = new Scene(root);
		var resource = getClass().getResource("application.css");
		scene.getStylesheets().add(resource.toExternalForm());
		stage.setScene(scene);
		stage.setTitle("Invitation parser");
		stage.show();

	}
}
