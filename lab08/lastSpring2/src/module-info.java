module lastSpring2 {
	requires java.xml.bind;
	requires java.desktop;
	requires java.annotation;
	requires spring.context;
	requires spring.beans;
	requires spring.ws.core;
	requires spring.boot;
	requires spring.xml;
	requires spring.core;
	requires spring.boot.autoconfigure;
}