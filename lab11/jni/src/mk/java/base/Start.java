package mk.java.base;

import java.util.Random;

public class Start {
	public static void main(String[] args) {
		showcase();
	}
	
	private static void showcase() {
		System.loadLibrary("counterDLL");
		var counter = new Counter();
		var res = counter.sort01(rand(20), false);
		System.out.println(valid(res, false));

		counter.order = false;
		res = counter.sort02(rand(20));
		System.out.println(valid(res, false));
		
		counter.a = rand(20);
		counter.order = false;
		counter.sort03();
		System.out.println(valid(counter.b, false));
	}
	
	private static boolean valid(Double[] arr, boolean rising) {
		for(int it = 1; it < arr.length; ++it) {
			if(rising) {
				if (arr[it] < arr[it-1]) {
					return false;
				}
			} else {
				if (arr[it] > arr[it-1]) {
					return false;
				}
			}
		}
		return true;
	}
	
	private static Double[] rand(int n) {
		var retArray = new Double[n];
		Random r = new Random();
		for(int it = 0; it < n; ++it) {
			retArray[it] = -1000 + (1000 - (-1000)) * r.nextDouble();
		}
		return retArray;
	}
}
