#include "pch.h"
#include "..\..\..\jni\bin\c\mk_java_base_Counter.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

std::vector<double> getVec(JNIEnv* env, jobjectArray arr) {
	int vecSize = env->GetArrayLength(arr);
	std::vector<double> resarr;
	jmethodID getValueDouble = env->GetMethodID(env->FindClass("java/lang/Double"), "doubleValue", "()D");
	for (int it = 0; it < vecSize; ++it) {
		double val = env->CallDoubleMethod(env->GetObjectArrayElement(arr, it), getValueDouble);
		resarr.push_back(val);
	}
	return resarr;
}

JNIEXPORT jobjectArray JNICALL Java_mk_java_base_Counter_sort01
(JNIEnv* env, jobject obj, jobjectArray arr, jobject obj2) {
	int vecSize = env->GetArrayLength(arr);
	std::vector<double> resarr;
	jclass dblClass = env->FindClass("java/lang/Double");
	jmethodID getValueDouble = env->GetMethodID(dblClass, "doubleValue", "()D");
	for (int it = 0; it < vecSize; ++it) {
		jobject element = env->GetObjectArrayElement(arr, it);
		double val = env->CallDoubleMethod(element, getValueDouble);
		resarr.push_back(val);
	}
	bool asc = env->CallBooleanMethod(obj2, env->GetMethodID(env->FindClass("java/lang/Boolean"), "booleanValue", "()Z"));
	if (asc == true) {
		std::sort(resarr.begin(), resarr.end());
	}
	else {
		std::sort(resarr.begin(), resarr.end(), std::greater<double>());
	}
	jobjectArray objArr = env->NewObjectArray(vecSize, dblClass, 0);
	jmethodID doubleConstr = env->GetMethodID(dblClass, "<init>", "(D)V");
	for (int it = 0; it < vecSize; ++it) {
		jobject dbl = env->NewObject(dblClass, doubleConstr, resarr[it]);
		env->SetObjectArrayElement(objArr, it, dbl);
	}
	return objArr;
}


JNIEXPORT jobjectArray JNICALL Java_mk_java_base_Counter_sort02
(JNIEnv* env, jobject obj, jobjectArray arr) {
	int vecSize = env->GetArrayLength(arr);
	std::vector<double> resarr;
	jclass dblClass = env->FindClass("java/lang/Double");
	jmethodID getValueDouble = env->GetMethodID(dblClass, "doubleValue", "()D");
	for (int it = 0; it < vecSize; ++it) {
		double val = env->CallDoubleMethod(env->GetObjectArrayElement(arr, it), getValueDouble);
		resarr.push_back(val);
	}
	jfieldID fieldID = env->GetFieldID(env->GetObjectClass(obj), "order", "Ljava/lang/Boolean;");
	jobject booleanObj = env->GetObjectField(obj, fieldID);
	bool asc = env->CallBooleanMethod(booleanObj, env->GetMethodID(env->FindClass("java/lang/Boolean"), "booleanValue", "()Z"));
	if (asc == true) {
		std::sort(resarr.begin(), resarr.end());
	}
	else {
		std::sort(resarr.begin(), resarr.end(), std::greater<double>());
	}
	jobjectArray objArr = env->NewObjectArray(vecSize, dblClass, 0);
	jmethodID doubleConstr = env->GetMethodID(dblClass, "<init>", "(D)V");
	for (int it = 0; it < vecSize; ++it) {
		jobject dbl = env->NewObject(dblClass, doubleConstr, resarr[it]);
		env->SetObjectArrayElement(objArr, it, dbl);
	}
	return objArr;
}

JNIEXPORT void JNICALL Java_mk_java_base_Counter_sort03
(JNIEnv* env, jobject obj) {
	std::vector<double> arr;
	std::string input("h");
	while (input.compare("0")) {
		std::cout << "Input(0 terminates data entry): ";
		std::cin >> input;
		arr.push_back(std::stoi(input));
	}
	std::cout << "Order(1/0): ";
	bool order;
	std::cin >> input;
	order = std::stoi(input);

	//ustawienie order
	jfieldID orderFieldID = env->GetFieldID(env->GetObjectClass(obj), "order", "Ljava/lang/Boolean;");
	env->SetBooleanField(obj, orderFieldID, order);


	//ustawienie a
	jclass dblClass = env->FindClass("java/lang/Double");
	jobjectArray objArr = env->NewObjectArray(arr.size(), dblClass, 0);
	jmethodID doubleConstr = env->GetMethodID(dblClass, "<init>", "(D)V");
	for (int it = 0; it < arr.size(); ++it) {
		jobject dbl = env->NewObject(dblClass, doubleConstr, arr[it]);
		env->SetObjectArrayElement(objArr, it, dbl);
	}
	jfieldID arrFieldID = env->GetFieldID(env->GetObjectClass(obj), "a", "[Ljava/lang/Double;");
	env->SetObjectField(obj, arrFieldID, objArr);


	//wywolanie sortowania w jvm
	jclass counterClass = env->GetObjectClass(obj);
	jmethodID sort04 = env->GetMethodID(counterClass, "sort04", "()V");
	env->CallVoidMethod(obj, sort04);
}

