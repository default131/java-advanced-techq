package mk.java.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.function.Consumer;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

public class Window {

	private JFrame frame;
	private SimPanel panel;

	public static interface Mediator {
		public void chooseScript(Consumer<int[][]> consumer);
	}
	
	/**
	 * Create the application.
	 */
	public Window(Mediator mediator) {
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Windows".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception e) {
		   
		}
		initialize(mediator);
		frame.setVisible(true);
	}
	
	public static Path chooseFile() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		if(fileChooser.showOpenDialog(fileChooser) == JFileChooser.APPROVE_OPTION) {
			Path dir = fileChooser.getSelectedFile().toPath();
			return dir;
		}
		return null;
	}
	
	public void updateBoard(int board[][]) {
		panel.setBoard(board);
	}
	
	private class SimPanel extends JPanel {
		private static final long serialVersionUID = 1L;
		public int[][] lastBoard = {{0, 1}};
		private final Color CL_EMPTY = Color.WHITE;
		private final Color CL_FULL = Color.BLACK;
		
		void setBoard(int[][] board) {
			lastBoard = Arrays.stream(board).map(el -> el.clone()).toArray($ -> board.clone());
		}
		
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			var planeSize = this.getSize();
			var smaller = planeSize.height > planeSize.width?
					planeSize.width : planeSize.height;
			var boardSize = lastBoard.length;
			int sqrSize = smaller/boardSize;
			for (int row = 0; row < boardSize; ++row) {
				for (int col = 0; col < boardSize; ++col) {
					g.setColor(lastBoard[row][col] > 0? CL_FULL : CL_EMPTY);
					g.fillRect(sqrSize * col, sqrSize * row, sqrSize, sqrSize);
				}
			}
			repaint();
		}
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Mediator mediator) {
		frame = new JFrame();
		frame.setBounds(100, 100, 798, 505);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		panel = new SimPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		
		JPanel panel_1 = new JPanel();
		frame.getContentPane().add(panel_1, BorderLayout.WEST);
		
		JButton btnScript = new JButton("Choose script");
		btnScript.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnScript.addActionListener(event -> {
			mediator.chooseScript(panel::setBoard);
		});
		panel_1.add(btnScript);
	}

}
