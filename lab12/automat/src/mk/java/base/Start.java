package mk.java.base;

import mk.java.gui.Window;

public class Start {
	public static void main(String[] args) {
		new Window(new SimExecutor(args.length > 0?
				Integer.valueOf(args[0]) : 100));
	}
}
