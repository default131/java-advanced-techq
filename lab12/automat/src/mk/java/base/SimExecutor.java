package mk.java.base;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.function.Consumer;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import jdk.nashorn.api.scripting.ScriptObjectMirror;
import mk.java.gui.Window;
import mk.java.gui.Window.Mediator;

public class SimExecutor implements Mediator{
	private int gameSize;
	private Invocable engine;
	private Thread game;
	
	public SimExecutor(int gameSize) {
		this.gameSize = gameSize;
	}
	
	@Override 
	public void chooseScript(Consumer<int[][]> updater) {
		var filePath = Window.chooseFile();
		if(filePath == null) {
			if(game != null) {
				game.interrupt();
			}
			return;
		}
		ScriptEngineManager engineManager = new ScriptEngineManager();
		ScriptEngine engine = engineManager.getEngineByName("nashorn");
		try {
			engine.eval(new FileReader(filePath.toString()));
		} catch (FileNotFoundException | ScriptException e) {
			e.printStackTrace();
		}
		
	    this.engine = (Invocable) engine;
	    startSim(updater);
	}
	
	private void startSim(Consumer<int[][]> updater) {
		game = new Thread(() -> {
			try {
				engine.invokeFunction("initBoard", gameSize);
				while(true) {
					int[][] res = (int[][]) ((ScriptObjectMirror)engine.invokeFunction("evalBoard"))
							.to(int[][].class);
					updater.accept(res);
					Thread.sleep(32);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		game.start();
	}

}
