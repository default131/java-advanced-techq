gboard = []
function initBoard(size){
  //board = [[-1,-1,-1,-1,-1,-1],
  //  [-1,-1,-1,-1,-1,-1],
  //  [-1,-1,1,1,1,-1],
  //  [-1,1,1,1,-1,-1],
  //  [-1,-1,-1,-1,-1,-1],
  //  [-1,-1,-1,-1,-1,-1]];
  board = []
  for(row = 0; row < size; ++row) {
    rowBuff = [];
    for(col = 0; col < size; ++col) {
      rowBuff.push(Math.round((Math.random()*2 -1)))
    }
    board.push(rowBuff);
  }
  gboard = board;
};

function initArr2d(size) {
  boardtemp = [];

  for(row = 0; row < size; ++row) {
    rowBuff = [];
    for(col = 0; col < size; ++col) {
      rowBuff.push(-1)
    }
    boardtemp.push(rowBuff);
  }

  return boardtemp;
}

function evalBoard() {
  board = gboard;
  bufferBoard = initArr2d(board.length)
  for (row = 0; row < board.length; ++row) {
    for (col = 0; col < board[0].length; ++col) {
      liveNeighbours = countLiveNeighbours(board, row, col)
      if (liveNeighbours < 2 || liveNeighbours > 3) {//1,3
        bufferBoard[row][col] = -1;
      } else {
        if (liveNeighbours == 2 && board[row][col] == -1) {
          bufferBoard[row][col] = -1;
        } else {
          bufferBoard[row][col] = liveNeighbours;
        }
      }
    }
  }
  gboard = bufferBoard
  return board;
};

function countLiveNeighbours(board, row, col) {
  neighbourCount = 0;  
  if (row > 0 && col > 0) {if (board[row-1][col-1] > 0) {++neighbourCount;}} //NW
  if (row > 0) {if (board[row-1][col] > 0) {++neighbourCount;}} //N
  if (row > 0 && col < board[0].length-1) {if (board[row-1][col+1] > 0) {++neighbourCount;}} //NE
  if (col < board[0].length-1) {if (board[row][col+1] > 0) {++neighbourCount;}} //E
  if (row < board.length-1 && col < board[0].length-1) {if (board[row+1][col+1] > 0) {++neighbourCount;}} //SE
  if (row < board.length-1) {if (board[row+1][col] > 0) {++neighbourCount;}} //S
  if (row < board.length-1 && col > 0) {if (board[row+1][col-1] > 0) {++neighbourCount;}} //SW
  if (col > 0) {if (board[row][col-1] > 0) {++neighbourCount;}} //W
  return neighbourCount;
};
