# Lab12 - Java + Javascript
## Użycie
Aby wybrać skrypt z zaimplementowanymi metodami symulacji, należy kliknąć przycisk *Choose script* i następnie wybrać plik. Symulacja zostanie uruchomiona.
![usg1](usage_1.png)

![usgSel](usage_sel.png)

![sym_gif](usage.gif)

W celu zatrzymania symulacji należy ponownie zainicjować wybór pliku, wybierając opcję *cancel* w oknie selektora. Zatrzyma to prezentację symulacji na ostatnim stanie.
![usg2](usage_2.png)

Możliwe jest użycie różnych implementacji, poniżej Gra w Życie z definicją sąsiedztwa Neumann'a

![usg3](usage_3.png)